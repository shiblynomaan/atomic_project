<?php

namespace App\BITM\SEIP1020\Birthday;

use \App\BITM\SEIP1020\Utility\Utility;

class Birthday {
    
    public $id = "";
    public $bday = "";
    public $name = "";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
    // public $deleted_at = ""; //soft delete
    
    public function __construct($data = false){
         if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }

        $this->bday = $data['bday'];
        $this->name = $data['name'];
    }
    
    public function index(){
        
        $birthdays = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "SELECT * FROM `birthdays`";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $birthdays[] = $row;
        }
        return $birthdays;
    }
    
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "INSERT INTO `atomicproject`.`birthdays` ( `bday`,`name`) VALUES ( '".$this->bday."','".$this->name."')";
 
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Birthday is added successfully.");
        }else{
            Utility::message("There is an error while saving data.");
        }
        
        Utility::redirect('index.php');
    }
     public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `atomicproject`.`birthdays` WHERE `birthdays`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Birthday is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    public function show($id=false){
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query="SELECT * FROM `birthdays` WHERE id=".$id;
        
        $result = mysql_query($query);
        
        $row = mysql_fetch_object($result);
        
        return $row;
            
    }
     public function update(){
          
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query ="UPDATE `atomicproject`.`birthdays` SET `bday` = '".$this->bday."', `name` = '".$this->name."' WHERE `birthdays`.`id` = ".$this->id;
       
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Birthday is Edited successfully.");
        }else{
            Utility::message("There is an error while Editing data.");
        }
        
        Utility::redirect('index.php');
    }
  
 }






