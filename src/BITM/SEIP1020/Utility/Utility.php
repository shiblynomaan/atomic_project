<?php

namespace App\BITM\SEIP1020\Utility;

class Utility{
    public $message="";
    
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function message($message=null){
        
        if(is_null($message)){//pls giv me message
            
          $message= self::getMessage();
            return $message;
        }else{ //pls set this message
            self::setMessage($message);
        }                       
    
    }
    static private function getMessage(){
        
        $_message = $_SESSION['message'];
        $_SESSION['message']="";
        return $_message;
     
    }
    
     static private function setMessage($message){
         $_SESSION['message']=$message;

    }
       
    static public function redirect($url="/AtomicProject/Views/SEIP1020/Book/index.php"){
        header("Location:".$url);
    }
    static public function redirect1($url="/AtomicProject/Views/SEIP1020/Birthday/index.php"){
        header("Location:".$url);
    }
    static public function redirect2($url="/AtomicProject/Views/SEIP1020/Email/index.php"){
        header("Location:".$url);
    }
      static public function redirect3($url="/AtomicProject/Views/SEIP1020/Summary/index.php"){
        header("Location:".$url);
    }
}