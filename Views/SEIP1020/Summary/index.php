<?php
    include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    
    use \App\BITM\SEIP1020\Summary\Summary;
    use \App\BITM\SEIP1020\Utility\Utility;
    
    $summary = new Summary();
    $summarys = $summary->index();
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
              #message{background-color: green}
            
        </style>
    </head>
    <body>
        <h1>Summary Of organization</h1>
        <div id="message">
            <?php echo Utility::message(); ?>
        </div>
        <div><span>Search / Filter </span> 
            <span id="utility">Download as PDF | XL  <a href="create.php">Add New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>ID</th>
                    <th>Summary &dArr;</th>
                     <th>Organization Name &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $slno =1;
               foreach($summarys as $summary){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                    <td><?php echo $summary->id;?></td>
                    <td><a href="#"><?php echo $summary->summary;?></a></td>
                    <td><?php echo $summary->name;?></td>
                    <td><a href="show.php?id=<?php echo $summary->id;?>">View</a>
                        |  <a href="edit.php?id=<?php echo $summary->id;?>">Edit</a>
                        | Delete
                        <form action="delete.php" method="post">
                         <input type="hidden" name ="id" value="<?php echo $summary->id;?>">
                        <button type="submit" class="delete">Delete</button>
                        </form>
                        | Trash/Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
            }
            ?>
            </tbody>
        </table>
        <div><span> prev  1 | 2 | 3 next </span></div>
        <script src="https://code.jquery.com/jquery-1.11.3.min.js" type="text/javascript" ></script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           });
            
         </script>
    </body>
</html>






