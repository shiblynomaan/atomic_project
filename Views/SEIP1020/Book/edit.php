<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \App\BITM\SEIP1020\Book\Book;
use \App\BITM\SEIP1020\Utility\Utility;

$obj = new Book();

$book=$obj->show($_GET['id']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="post">
            <fieldset>
                <legend>Edit Book</legend>
                <input autofocus="autofocus" 
                    
                           placeholder="Enter the title of your favorite book" 
                           type="hidden" 
                           name="id"
                           required="required"
                           value="<?php echo $book->id;?>"
                      
                           />
                <div>
                    <label>Enter Book Title</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter the title of your favorite book" 
                           type="text" 
                           name="title"
                     
                           required="required"
                           value="<?php echo $book->title;?>"
                      
                           />
                 </div>
                <div>
                    <label>Enter Author</label>
                    <input placeholder="Enter author name" 
                           type="text" 
                           name="author"
                           required="required"
                           value="<?php echo $book->author;?>"
                           />
                </div>
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>



