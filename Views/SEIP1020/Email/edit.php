<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \App\BITM\SEIP1020\Email\email;
use \App\BITM\SEIP1020\Utility\Utility;

$obj = new Email();

$email=$obj->show($_GET['id']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="post">
            <fieldset>
                <legend>Edit Email</legend>
                <input autofocus="autofocus" 
                    
                           placeholder="Enter email" 
                           type="hidden" 
                           name="id"
                           required="required"
                           value="<?php echo $email->id;?>"
                      
                           />
                <div>
                    <label>Enter Email</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter email" 
                           type="email" 
                           name="email"
                     
                           required="required"
                           value="<?php echo $email->email;?>"
                      
                           />
                 </div>
                <div>
                    <label>Enter Name</label>
                    <input placeholder="Enter name" 
                           type="text" 
                           name="name"
                           required="required"
                           value="<?php echo $email->name;?>"
                           />
                </div>
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>



