<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \App\BITM\SEIP1020\Email\Email;
use \App\BITM\SEIP1020\Utility\Utility;

$obj = new Email();

$email=$obj->show($_GET['id']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
    <body>
<h1>Book Detail</h1>

<dl>
    <dt>Id</dt>
    <dd><?php echo $email->id; ?></dd>
    
    <dt>Title</dt>
    <dd><?php echo $email->email; ?></dd>
    
    <dt>Author</dt>
    <dd><?php echo $email->name; ?></dd>
</dl>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>

    </body>
</html>