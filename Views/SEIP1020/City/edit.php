<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \App\BITM\SEIP1020\City\City;
use \App\BITM\SEIP1020\Utility\Utility;

$obj = new City();

$city=$obj->show($_GET['id']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="post">
            <fieldset>
                <legend>Edit City Name</legend>
                <input autofocus="autofocus" 
                    
                           placeholder="Enter City Name" 
                           type="hidden" 
                           name="id"
                           required="required"
                           value="<?php echo $city->id;?>"
                      
                           />
                <div>
                    <label>Enter City Name</label>
                    <input
                        value="<?php echo $city->cname;?>"
                        />
                    <select name="cname" value="<?php echo $city->cname;?>">
                      
                <option>Dhaka</option>
                <option>Bogura</option>
                <option>Rajshahi</option>
                <option>Chittagong</option>
                <option>Comilla</option>
                
            </select>
                    
                 </div>
                <div>
                    <label>Enter Name</label>
                    <input placeholder="Enter name" 
                           type="text" 
                           name="name"
                           required="required"
                           value="<?php echo $city->name;?>"
                           />
                </div>
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>



