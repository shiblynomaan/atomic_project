<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \App\BITM\SEIP1020\Birthday\Birthday;
use \App\BITM\SEIP1020\Utility\Utility;

$obj = new Birthday();

$birthday=$obj->show($_GET['id']);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
    <body>
<h1>Birthday</h1>

<dl>
    <dt>Id</dt>
    <dd><?php echo $birthday->id; ?></dd>
    
    <dt>Bday</dt>
    <dd><?php echo $birthday->bday; ?></dd>
    
    <dt>name</dt>
    <dd><?php echo $birthday->name; ?></dd>
</dl>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>

    </body>
</html>